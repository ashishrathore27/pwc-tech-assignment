package com.pwc.techassigment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechAssigmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechAssigmentApplication.class, args);
	}

}
