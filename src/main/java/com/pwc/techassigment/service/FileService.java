package com.pwc.techassigment.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.GsonBuilder;
import com.pwc.techassigment.model.*;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class FileService {

    private static final ObjectMapper OBJECT_MAPPER;

    static {
        OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public FileSystemResource process(MultipartFile multipartFile) throws IOException {
        List<Line> lines = OBJECT_MAPPER
                .readValue(multipartFile.getBytes(), FileRequest.class)
                .getLines();
        List<Line> left = new ArrayList<>();
        List<Line> right = new ArrayList<>();
        List<Double> leftPointList = getLeftPoints.apply(lines);

        divideListIntoLeftAndRight(lines, left, right, leftPointList);

        List<String> finalLeftLines = new ArrayList<>(getLineTextFromTop(left).values());

        List<String> finalRightLines = new ArrayList<>(getLineTextFromTop(right).values());

        return new FileSystemResource(getFile(finalLeftLines, finalRightLines));
    }

    public File getFile(List<String> finalLeftLines, List<String> finalRightLines) throws IOException {
        FileResponse fileResponse = new FileResponse();
        fileResponse.setLeft(finalLeftLines);
        fileResponse.setRight(finalRightLines);
        File output = File.createTempFile("output", ".json");
        Files.write(Paths.get(output.getPath()), new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(fileResponse).getBytes());
        return output;
    }

    private Map<Double, String> getLineTextFromTop(List<Line> left) {
        Map<Double, String> finalLines = new TreeMap<>();
        Map<Double, List<Line>> wordAccordingTop = left.stream().collect(Collectors.groupingBy(line ->
                                Math.floor(line.getGeometry().getBoundingBox().getTop() * 100) / 100));
        wordAccordingTop.forEach((aDouble, lines1) -> {
            String lineString = lines1.stream()
                    .sorted(Comparator.comparing(line -> line.getGeometry().getBoundingBox().getLeft()))
                    .map(Line::getText)
                    .collect(Collectors.joining(" "));
            finalLines.put(aDouble, Arrays.stream(lineString.split(" ")).distinct().collect(Collectors.joining(" ")));
        });
        return finalLines;
    }

    private void divideListIntoLeftAndRight(List<Line> lines, List<Line> left, List<Line> right, List<Double> leftPointList) {
        int length = leftPointList.size();
        Double startOfLeft = leftPointList.get(0);
        Double endOfLeft = leftPointList.get(length - 1);
        Double middleOfLeft = leftPointList.get(length / 2 - 1);

        lines.forEach(line -> {
            if (line.getGeometry().getBoundingBox().getLeft() >= startOfLeft &&
                    line.getGeometry().getBoundingBox().getLeft() <= middleOfLeft) left.add(line);
            else if (line.getGeometry().getBoundingBox().getLeft() >= middleOfLeft &&
                    line.getGeometry().getBoundingBox().getLeft() <= endOfLeft) right.add(line);
        });
    }

    private final Function<List<Line>, List<Double>> getLeftPoints = lines -> lines.stream()
            .map(Line::getGeometry)
            .map(Geometry::getBoundingBox)
            .map(BoundingBox::getLeft)
            .sorted(Comparator.naturalOrder())
            .collect(Collectors.toList());

}
