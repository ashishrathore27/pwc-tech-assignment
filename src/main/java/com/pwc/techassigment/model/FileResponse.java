package com.pwc.techassigment.model;

import java.util.List;

public class FileResponse {
    private List<String> left;
    private List<String> right;

    public List<String> getLeft() {
        return left;
    }

    public void setLeft(List<String> left) {
        this.left = left;
    }

    public List<String> getRight() {
        return right;
    }

    public void setRight(List<String> right) {
        this.right = right;
    }

}
