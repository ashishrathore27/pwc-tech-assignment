package com.pwc.techassigment.model;

import java.util.ArrayList;

public class Line {
    private String blockType;
    private float confidence;
    private String text;
    Geometry geometry;
    private String Id;
    ArrayList<Object> relationships = new ArrayList<Object>();

    public String getBlockType() {
        return blockType;
    }

    public float getConfidence() {
        return confidence;
    }

    public String getText() {
        return text;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public String getId() {
        return Id;
    }

    public void setBlockType(String BlockType) {
        this.blockType = BlockType;
    }

    public void setConfidence(float Confidence) {
        this.confidence = Confidence;
    }

    public void setText(String Text) {
        this.text = Text;
    }

    public void setGeometry(Geometry GeometryObject) {
        this.geometry = GeometryObject;
    }

    public void setId(String Id) {
        this.Id = Id;
    }
}


