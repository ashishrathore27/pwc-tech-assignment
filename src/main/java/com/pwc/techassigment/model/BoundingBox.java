package com.pwc.techassigment.model;

public class BoundingBox {
    private Double width;
    private Double height;
    private Double left;
    private Double top;

    public Double getWidth() {
        return width;
    }

    public Double getHeight() {
        return height;
    }

    public Double getLeft() {
        return left;
    }

    public Double getTop() {
        return top;
    }

    public void setWidth(Double Width) {
        this.width = Width;
    }

    public void setHeight(Double Height) {
        this.height = Height;
    }

    public void setLeft(Double Left) {
        this.left = Left;
    }

    public void setTop(Double Top) {
        this.top = Top;
    }
}
