package com.pwc.techassigment.model;

import java.util.ArrayList;

public class Geometry {
    BoundingBox boundingBox;
    ArrayList< Object > polygon = new ArrayList < Object > ();

    public BoundingBox getBoundingBox() {
        return boundingBox;
    }

    public void setBoundingBox(BoundingBox BoundingBoxObject) {
        this.boundingBox = BoundingBoxObject;
    }

}
