package com.pwc.techassigment.model;

import java.util.List;

public class FileRequest {
    private List<Line> lines;
    private String analyzeDocumentModelVersion;

    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }

    public String getAnalyzeDocumentModelVersion() {
        return analyzeDocumentModelVersion;
    }

    public void setAnalyzeDocumentModelVersion(String analyzeDocumentModelVersion) {
        this.analyzeDocumentModelVersion = analyzeDocumentModelVersion;
    }
}
