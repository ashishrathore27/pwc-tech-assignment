package com.pwc.techassigment.resource;

import com.pwc.techassigment.service.FileService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/file")
public class FileResource {

    private final FileService fileService;

    public FileResource(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping
    public FileSystemResource process(@RequestParam(value = "file") MultipartFile multipartFile) throws IOException {
        return fileService.process(multipartFile);
    }
}
