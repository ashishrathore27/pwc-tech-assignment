package com.pwc.techassigment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.io.IOException;

@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(IOException.class)
    public ResponseEntity ioException(IOException exception){
        return new ResponseEntity("please provide valid input file", HttpStatus.BAD_REQUEST);
    }
}
